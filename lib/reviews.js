'use strict';

const R = require('ramda');
const common = require('./common');
const app = require('./app');
const c = require('./constants');

function cleanList (results) {
  const reviews = results.feed.entry || [];
  console.log("REVIEW : ", reviews);
  return reviews.slice(1).map((review) => ({
    id: review.id[0],
    userName: review.author.name,
    userUrl: review.author.uri,
    version: review['im:version'],
    score: parseInt(review['im:rating'][0]),
    title: review.title[0],
    text: review.content[0],
    // url: review.link.attributes.href,
    date: review.updated[0]
  }));
}

const reviews = (opts) => new Promise((resolve) => {
  validate(opts);

  if (opts.id) {
    resolve(opts.id);
  } else if (opts.appId) {
    resolve(app(opts).then(app => app.id));
  }
})
.then((id) => {
  opts = opts || {};
  opts.sort = opts.sort || c.sort.RECENT;
  opts.page = opts.page || 1;
  opts.country = opts.country || 'us';

  const url = `https://itunes.apple.com/${opts.country}/rss/customerreviews/page=${opts.page}/id=${id}/sortby=${opts.sort}/xml`;
  return common.request(url).then(body => {
      const util = require('util');
      const parseString = util.promisify(require('xml2js').parseString);
      // console.log("REVIEW XML :", url, body, url);
      return parseString(body).then(result => {
          // console.log("REVIEW XML->JSON :", result);
          return result;
      });
  });
})
// .then(JSON.parse)
.then(cleanList);

function validate (opts) {
  if (!opts.id && !opts.appId) {
    throw Error('Either id or appId is required');
  }

  if (opts.sort && !R.contains(opts.sort, R.values(c.sort))) {
    throw new Error('Invalid sort ' + opts.sort);
  }

  if (opts.page && opts.page < 1) {
    throw new Error('Page cannot be lower than 1');
  }

  if (opts.page && opts.page > 10) {
    throw new Error('Page cannot be greater than 10');
  }
}

module.exports = reviews;
